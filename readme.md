# @fnet/npm-package-extract

## Introduction

The `@fnet/npm-package-extract` is a simple utility designed to fetch and extract npm packages directly from the npm registry. It allows users to download any version of a specified npm package, extract its content, and optionally install its dependencies. This tool can be useful for developers who need offline access to package contents or want to explore the package structure without the need to use it directly in their project immediately.

## How It Works

The utility works by first fetching the metadata of a specified npm package from the npm registry. It checks for the available versions and either selects the latest or a specific version as requested by the user. Once identified, it downloads the corresponding tarball file of the package, extracts it to a specified directory, and optionally renames the extracted directory to a more descriptive name. Users can also choose to keep or remove the downloaded tarball and install package dependencies if desired.

## Key Features

- Fetches metadata and tarballs directly from the npm registry.
- Supports downloading a specific version or the latest version of a package.
- Extracts the package contents to a specified directory.
- Allows for optional renaming of extracted contents for clarity.
- Provides options to retain the original tarball file.
- Can automatically install package dependencies excluding dev dependencies.

## Conclusion

The `@fnet/npm-package-extract` tool provides a straightforward method for extracting and managing npm packages locally. It's a handy utility for developers who need more control over package assets on their file system or require its structure offline. Its functionality is simple to use, allowing users to seamlessly integrate it into their workflows without much hassle.