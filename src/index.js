import fetch from 'node-fetch';
import * as tar from 'tar';
import { createWriteStream, rmSync, renameSync, existsSync } from 'node:fs';
import semver from 'semver';
import path from 'node:path';
import shell from 'shelljs';
import fs from 'node:fs';

export default async ({
  name,
  ver: version = 'latest',
  keep = false,
  output,
  install = false,
  rename = true
}) => {
  try {

    output = output || process.cwd();
    output = path.resolve(output);

    // create if not exists recursively
    if (!fs.existsSync(output)) {
      fs.mkdirSync(output, { recursive: true });
    }

    // Construct npm registry URL
    const registryUrl = `https://registry.npmjs.org/${name}`;

    // Fetch package metadata from npm registry
    const packageMeta = await fetch(registryUrl).then(response => response.json());
    if (!packageMeta.versions) throw new Error(`Package ${name} not found`);

    // Find the matching version or use the latest
    const versions = Object.keys(packageMeta.versions);
    const versionToDownload = version === 'latest' ? packageMeta['dist-tags'].latest : semver.maxSatisfying(versions, version);
    if (!versionToDownload) throw new Error(`Version ${version} not found for package ${name}`);

    // Define download path after knowing the exact version to download
    const packageDirName = `${name.replace('/', '-')}-${versionToDownload}`;
    const downloadPath = path.join(output, `${packageDirName}.tgz`);

    const tarballUrl = packageMeta.versions[versionToDownload].dist.tarball;

    // Download the tarball file
    const response = await fetch(tarballUrl);
    if (!response.ok) throw new Error(`Error fetching tarball: ${response.statusText}`);

    const tarballStream = response.body.pipe(createWriteStream(downloadPath));

    // Ensure the tarball file is fully downloaded before extracting
    await new Promise((resolve, reject) => {
      tarballStream
        .on('finish', resolve)
        .on('error', reject);
    });

    // Extract the tarball file
    await tar.x({
      file: downloadPath,
      C: output
    });

    const extractedPath = path.join(output, 'package');
    const newPath = rename ? path.join(output, packageDirName) : extractedPath;

    // Rename the extracted directory to the version name if rename is true
    if (rename && extractedPath !== newPath) {
      if (existsSync(newPath)) {
        rmSync(newPath, { recursive: true, force: true });
      }

      renameSync(extractedPath, newPath);
    }

    console.log(`${name}@${versionToDownload} is extracted to ${newPath}.`);

    // Remove the tarball file if keep is false
    if (!keep) {
      rmSync(downloadPath);
    }

    // Run npm install if install is true
    if (install) {
      shell.exec(`npm install --omit=dev`, { cwd: newPath });
    }

    return {
      output: newPath,
      name: name,
      version: versionToDownload,
      tarball: downloadPath,
    };

  } catch (error) {
    console.error('An error occurred:', error);
    throw error; // Rethrow the error to make sure it's caught by the caller
  }
};