# @fnet/npm-package-extract Developer Guide

## Overview

`@fnet/npm-package-extract` is a utility library designed to simplify the process of downloading and extracting npm packages. Developers can use this library to programmatically fetch specific versions of npm packages, extract their contents, and optionally install their dependencies. This can be particularly useful for managing package contents outside the typical npm workflow, such as in scenarios where package exploration or customized installations are needed.

## Installation

The library can be installed via npm or yarn. Use the following commands:

Using npm:
```bash
npm install @fnet/npm-package-extract
```

Using yarn:
```bash
yarn add @fnet/npm-package-extract
```

## Usage

The library exports a single default function, which facilitates the fetching and extraction process. Below is a basic guide on how to implement it:

```javascript
import npmPackageExtract from '@fnet/npm-package-extract';

(async () => {
  try {
    const result = await npmPackageExtract({
      name: 'express', // Name of the package
      ver: '4.17.1',   // Specific version, or 'latest'
      keep: false,     // Determines whether to keep the tarball
      output: './downloads',  // Output directory for the extracted contents
      install: true,   // If true, installs dependencies in the extracted directory
      rename: true     // Renames the extracted directory
    });

    console.log(`Package extracted to: ${result.output}`);
  } catch (error) {
    console.error('Extraction failed:', error);
  }
})();
```

## Examples

### Example 1: Basic Extraction

This example demonstrates a straightforward extraction of an npm package:

```javascript
import npmPackageExtract from '@fnet/npm-package-extract';

npmPackageExtract({
  name: 'lodash',
  ver: 'latest',
  keep: false,
  output: './lodash-package'
}).then(result => {
  console.log(`Lodash extracted to: ${result.output}`);
}).catch(console.error);
```

### Example 2: Extract and Install Dependencies

This example shows how to extract a package and install its dependencies:

```javascript
import npmPackageExtract from '@fnet/npm-package-extract';

npmPackageExtract({
  name: 'react',
  ver: '^17.0.0',
  install: true,  // Installs package dependencies
  output: './react-package'
}).then(result => {
  console.log(`React extracted and dependencies installed at: ${result.output}`);
}).catch(console.error);
```

## Acknowledgement

We acknowledge the `node-fetch`, `tar`, `semver`, and `shelljs` libraries for enabling core functionalities such as making HTTP requests, handling file extractions, semantic versioning, and shell command execution. These utilities underlie the robust operations of the `@fnet/npm-package-extract` library.